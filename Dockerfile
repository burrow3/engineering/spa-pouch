FROM node:18-alpine3.18
COPY . /var/task/
WORKDIR /var/task/
RUN yarn install
CMD ["yarn", "start"]