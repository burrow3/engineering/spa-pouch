import React from 'react'
import { Route, Switch } from 'react-router-dom'
import { DisplayUsers } from 'src/domain/Pouch/DisplayUsers'

const DisplayUsersRouter: React.FC = () => {
  return (
    <>
      <Switch>
        <Route path='/users' component={ DisplayUsers } />
      </Switch>
    </>
  )
}

export {
  DisplayUsersRouter
}
