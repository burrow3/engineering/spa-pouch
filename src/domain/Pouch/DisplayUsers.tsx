import React from 'react'
import { useQuery, gql } from '@apollo/client'

interface IUser {
  username?: String
  email?: String
  password?: String
}

function DisplayUsers () {
  const { loading, error, data } = useQuery(LIST_ALL_USERS)
  if (loading) {
    return <p>Loading...</p>
  }
  if (!data) {
    return (
      <p>No users</p>
    )
  }
  if (error) {
    return <p>Error : {error.message}</p>
  }

  return (
    <>
      {data.listAllUsers.map((user: IUser) => (
        <div key={'id'}>
          <h3>{user.username}</h3>
          <p>{user.email}</p>
          <br />
        </div>
      ))
      }
    </>
  )
}

const LIST_ALL_USERS = gql`
  query ListAllUsers {
    listAllUsers {
      username
      email
    }
  }
`

export { DisplayUsers }
