import React from 'react'
import './App.css'
import { Routers } from 'src/routers'

const App: React.FC = () => {
  return (
    <Routers />
  )
}

export { App }
