import { History, Location, createBrowserHistory } from 'history'
import { matchPath } from 'react-router-dom'
import { scrollToTop } from 'src/services/helpers/helpers'

let history: History
let previousLocation: Location

const scrollToTopRouteExceptions = [
  {
    routes: [
    ],
    params: []
  }
]

const shouldScrollToTopBetweenRoutes = (pathname: string, previousPathName: string) => {
  return scrollToTopRouteExceptions.every(({ routes, params }) => {
    const matchCurrent = matchPath(pathname, {
      path: routes,
      exact: true
    })

    const matchPast = matchPath(previousPathName, {
      path: routes,
      exact: true
    })

    if (!matchCurrent || !matchPast) {
      return true
    }

    // Compare the params - if there are changed params in the list of specified ones, we
    // also need to scroll to top
    return params.some((param) => {
      const currentParam = matchCurrent.params[param]
      const pastParam = matchPast.params[param]
      // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition
      return currentParam !== pastParam && !!pastParam && !!currentParam
    })
  })
}

const initializeHistory = () => {
  history = createBrowserHistory()
  previousLocation = history.location

  history.listen((location) => {
    const state = location.state as { resetScroll?: boolean } | undefined
    if ((state?.resetScroll ?? true) && location.pathname !== previousLocation.pathname && shouldScrollToTopBetweenRoutes(location.pathname, previousLocation.pathname)) {
      scrollToTop()
    }

    previousLocation = location
  })
}

const getHistory = () => {
  // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition
  if (!history) {
    initializeHistory()
  }

  return history
}

export {
  getHistory
}
