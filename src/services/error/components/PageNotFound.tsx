import React from 'react'

const PageNotFound: React.FC = () => {
  return (
    <>
      <h3>Page not found</h3>
      <p>This is unfortunate. The page you were looking for doesn&apos;t exist.</p>
    </>
  )
}

export { PageNotFound }
