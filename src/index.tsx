import React, { StrictMode } from 'react'
import { Location } from 'history'
import { createRoot } from 'react-dom/client'
import './index.css'
import { ApolloClient, InMemoryCache, ApolloProvider } from '@apollo/client'
import { PageNotFound } from 'src/services/error/components/PageNotFound'
import { Router, Route } from 'react-router-dom'
import { getHistory } from 'src/services/history/history'
import { App } from './App'
import { reportWebVitals } from './reportWebVitals'

const client = new ApolloClient({
  uri: 'http://localhost:8000/graphql',
  cache: new InMemoryCache()
})

const renderApp = ({ location }: { location: Location }) => {
  const state = location.state as {is404?: boolean} | undefined
  if (state?.is404) {
    state.is404 = false
    return (
      <PageNotFound />
    )
  }

  return (
    <App />
  )
}

// This const can be deleted when react router is upgraded to latest
// eslint-disable-next-line @typescript-eslint/no-explicit-any
const RouterAny: any = Router

const root = createRoot(document.getElementById('root') as HTMLElement)
root.render(
  <StrictMode>
    <ApolloProvider client={client}>
      <RouterAny history={getHistory()}>
        <Route render={renderApp} />
      </RouterAny>
    </ApolloProvider>
  </StrictMode>
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals

reportWebVitals()
