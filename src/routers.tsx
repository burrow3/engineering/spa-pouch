import React from 'react'
import { Route, Switch } from 'react-router-dom'
import { RedirectAs404 } from 'src/services/reactRouter/components/RedirectAs404'

const DisplayUserRouter = React.lazy(() => import('src/scenes/DisplayUser/Router').then(
  (module) => ({ default: module.DisplayUsersRouter })
))

const Routers: React.FC = () => {
  return (
    <Switch>
      <Route path='/users' component={DisplayUserRouter}></Route>
      <Route component={RedirectAs404} />
    </Switch>
  )
}

export { Routers }
