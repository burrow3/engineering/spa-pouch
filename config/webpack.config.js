'use strict';

const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const InlineChunkHtmlPlugin = require('react-dev-utils/InlineChunkHtmlPlugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { WebpackManifestPlugin } = require('webpack-manifest-plugin');
const InterpolateHtmlPlugin = require('react-dev-utils/InterpolateHtmlPlugin');
const ModuleNotFoundPlugin = require('react-dev-utils/ModuleNotFoundPlugin');
const NyanStyledProgressPlugin = require('nyan-styled-progress-webpack-plugin');
const TsConfigPathsPlugin = require('tsconfig-paths-webpack-plugin');
const ReactRefreshWebpackPlugin = require('@pmmmwh/react-refresh-webpack-plugin');
const paths = require('./paths');
const getClientEnvironment = require('./env');

module.exports = function(webpackEnv) {
  const isEnvProduction = webpackEnv === 'production';

  // Webpack uses `publicPath` to determine where the app is being served from.
  // It requires a trailing slash, or the file assets will get an incorrect path.
  // In development, we always serve from the root. This makes config easier.
  const publicPath = isEnvProduction ? paths.servedPath : '/';

  // `publicUrl` is just like `publicPath`, but we will provide it to our app
  // as %PUBLIC_URL% in `index.html` and `process.env.PUBLIC_URL` in JavaScript.
  // Omit trailing slash as %PUBLIC_URL%/xyz looks better than %PUBLIC_URL%xyz.
  const publicUrl = isEnvProduction ? publicPath.slice(0, -1) : '';

  // Get environment variables to inject into our app.
  const env = getClientEnvironment(publicUrl);

  return {
    // Chosen mode tells webpack to use its built-in optimizations accordingly.
    mode: isEnvProduction ? 'production' : 'development',
    // Stop compilation early in production
    bail: isEnvProduction,
    // enhance debugging by adding meta info for the browser devtools
    // source-map most detailed at the expense of build speed.
    devtool: isEnvProduction ? 'source-map' : 'cheap-module-source-map',
    // These are the "entry points" to our application.
    // This means they will be the "root" imports that are included in JS bundle.
    entry: [
      // We ship a few polyfills by default:
      require.resolve('core-js'),
      require.resolve('intersection-observer'),
      require.resolve('regenerator-runtime'),
      require.resolve('whatwg-fetch'),
      // Entry point to the application
      paths.appIndexJs,
    ].filter(Boolean),
    // lets you precisely control what bundle information gets displayed
    stats: isEnvProduction ? undefined : 'errors-only',
    // options related to how webpack emits results
    output: {
      // The build folder.
      // the target directory for all output files
      // must be an absolute path (use the Node.js path module)
      path: isEnvProduction ? paths.appBuild : undefined,
      // Add /* filename */ comments to generated require()s in the output.
      pathinfo: !isEnvProduction,
      crossOriginLoading: "anonymous",
      // There will be one main bundle, and one file per asynchronous chunk.
      // In development, it does not produce real files.
      filename: isEnvProduction
        ? `${paths.staticAssetPrefix}static/js/[name].[chunkhash:8].js`
        : `${paths.staticAssetPrefix}static/js/bundle.[chunkhash:8].js`,
      // There are also additional JS chunk files if you use code splitting.
      chunkFilename: isEnvProduction
        ? `${paths.staticAssetPrefix}static/js/[name].[chunkhash:8].chunk.js`
        : `${paths.staticAssetPrefix}static/js/[name].chunk.js`,
      // the url to the output directory resolved relative to the HTML page
      publicPath: publicPath,
      // Point sourcemap entries to original disk location (workaround to format as URL on Windows)
      devtoolModuleFilenameTemplate: isEnvProduction
        ? info =>
          path
            .relative(paths.appSrc, info.absoluteResourcePath)
            .replace(/\\/g, '/')
        : info => path.resolve(info.absoluteResourcePath).replace(/\\/g, '/')
    },
    optimization: {
      emitOnErrors: false,
      minimize: isEnvProduction,
      // minimizer can be an instance of TerserPlugin or a function as below
      minimizer: [
        (compiler) => {
          const TerserPlugin = require('terser-webpack-plugin');
          new TerserPlugin({
            terserOptions: {
              compress: {},
            }
          }).apply(compiler);
        }
      ],
      // include all types of chunks
      // 'all' will result in code splitting between node_modules and the spa code by default
      splitChunks: {
        chunks: 'all',
        name: false,
      },
      // Keep the runtime chunk separated to enable long term caching
      // https://twitter.com/wSokra/status/969679223278505985
      runtimeChunk: 'single',
    },
    resolve: {
      // Include polyfills here or disable them
      fallback: {
        path: require.resolve('path-browserify'),
        util: require.resolve('util/'),
        stream: false,
        crypto: false,
        buffer: false,
      },
      extensions: paths.moduleFileExtensions.map(ext => `.${ext}`),
      plugins: [
        new TsConfigPathsPlugin({ configFile: paths.appTsConfig }),
      ]
    },
    module: {
      // Error if a module fails to export anything
      strictExportPresence: true,
      rules: [
        {
          test: /node_modules\/vfile\/core\.js/,
          use: [{
            loader: 'imports-loader',
            options: {
              type: 'commonjs',
              imports: ['single process/browser process'],
            },
          }],
        },
        {
          test: /\.(js|jsx|ts|tsx)$/,
          include: [paths.appSrc],
          loader: require.resolve('babel-loader'),
          options: {
            // This is a feature of `babel-loader` for webpack (not Babel itself).
            // It enables caching results in ./node_modules/.cache/babel-loader/
            // directory for faster rebuilds.
            cacheDirectory: true,
            cacheCompression: isEnvProduction,
            compact: isEnvProduction,
            plugins: [!isEnvProduction && require.resolve('react-refresh/babel')].filter(Boolean),
          },
        },
        // In development, we use style-loader to inject css directly into the dom
        // In production, this is extracted into separate css files (this process does not work in dev-server, and is slower)
        // Ideally, we should be using CDNs for these css files to avoid having to process css in the build, so this is a stopgap solution.
        {
          test: /\.css$/,
          use: [
            !isEnvProduction && 'style-loader',
            isEnvProduction && {
              loader: MiniCssExtractPlugin.loader
            },
            'css-loader'
          ].filter(Boolean),
          sideEffects: true
        },
        {
          test: /\.ya?ml$/,
          use: [
            'json-loader',
            {
              loader: 'yaml-loader',
              options: { asJSON: true }
            }
          ]
        },
        // Copy other files into the public folder
        {
          test: /\.(svg|ttf|woff|woff2|eot)$/,
          loader: require.resolve('file-loader'),
          options: {
            name: `${paths.staticAssetPrefix}static/media/[name].[hash:8].[ext]`,
          }
        }
      ]
    },
    plugins: [
      // Show a progress bar when building in development mode
      !isEnvProduction &&
        new NyanStyledProgressPlugin({
          width: 50,
          nyanCatSays: (progress) => {
            if (progress < 1) {
              return `${(progress * 100).toFixed(2)}% Nyan~`;
            }

            return 'Nyan!';
          }
        }),
      // Enables HMR support for react / hot module replacement
      !isEnvProduction && new ReactRefreshWebpackPlugin(),
      // Generates an `index.html` file with the <script> injected.
      new HtmlWebpackPlugin(
        Object.assign(
          {},
          {
            inject: true,
            template: paths.appHtml,
          },
          isEnvProduction
            ? {
                minify: {
                  removeComments: true,
                  collapseWhitespace: true,
                  removeRedundantAttributes: true,
                  useShortDoctype: true,
                  removeEmptyAttributes: true,
                  removeStyleLinkTypeAttributes: true,
                  keepClosingSlash: true,
                  minifyJS: true,
                  minifyCSS: true,
                  minifyURLs: true,
                },
              }
            : undefined
        )
      ),
      // Inlines the webpack runtime script. This script is too small to warrant
      // a network request.
      isEnvProduction &&
      new InlineChunkHtmlPlugin(HtmlWebpackPlugin, [/runtime~.+[.]js/]),
      // Makes some environment variables available in index.html.
      // The public URL is available as %PUBLIC_URL% in index.html, e.g.:
      // <link rel="shortcut icon" href="%PUBLIC_URL%/favicon.ico">
      new InterpolateHtmlPlugin(HtmlWebpackPlugin, env.raw),
      // This gives some necessary context to module not found errors, such as
      // the requesting resource.
      new ModuleNotFoundPlugin(paths.appPath),
      // Makes some environment variables available to the JS code, for example:
      // if (process.env.NODE_ENV === 'production') { ... }. See `./env.js`.
      // It is absolutely essential that NODE_ENV is set to production
      // during a production build.
      // Otherwise React will be compiled in the very slow development mode.
      // allows you to create global constants which can be configured at compile time.
      // This can be useful for allowing different behavior between development builds and release builds.
      new webpack.DefinePlugin(env.stringified),
      isEnvProduction &&
        new MiniCssExtractPlugin({
          // Options similar to the same options in webpackOptions.output
          // both options are optional
          filename: `${paths.staticAssetPrefix}static/css/[name].[contenthash:8].css`,
          chunkFilename: `${paths.staticAssetPrefix}static/css/[name].[contenthash:8].chunk.css`,
        }),
      // Generate a manifest file which contains a mapping of all asset filenames
      // to their corresponding output file so that tools can pick it up without
      // having to parse `index.html`.
      new WebpackManifestPlugin({
        fileName: 'asset-manifest.json',
        publicPath: publicPath,
      }),
    ].filter(Boolean),
    // Turn off performance processing because we utilize
    // our own hints via the FileSizeReporter
    performance: {
      hints: false,
    },
  };
};
