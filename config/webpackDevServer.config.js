'use strict';

const errorOverlayMiddleware = require('react-dev-utils/errorOverlayMiddleware');
const evalSourceMapMiddleware = require('react-dev-utils/evalSourceMapMiddleware');
const noopServiceWorkerMiddleware = require('react-dev-utils/noopServiceWorkerMiddleware');
const ignoredFiles = require('react-dev-utils/ignoredFiles');
const paths = require('./paths');
const fs = require('fs');

const protocol = 'https';
const host = process.env.HOST || '0.0.0.0';
const hot = (process.env.HOT_RELOADING_ENABLED === 'true') || false

module.exports = function (proxy, allowedHost, port) {
  // https://webpack.js.org/configuration/dev-server/
  return {
    allowedHosts: 'all',
    client: {
      logging: 'info',
      overlay: false,
      webSocketURL: 'auto://0.0.0.0:0/ws'
    },
    // Enable gzip compression of generated files.
    compress: true,
    devMiddleware: {
      // We serve from the generated url for the server directly from the ip address
      publicPath: `${protocol}://${allowedHost}:${port}/`
    },
    // Enable HTTPS if the HTTPS environment variable is set to 'true'
    https: protocol === 'https',
    // Need to return this for CORS as requests to the public folder now go to the ip address rather than a local one
    headers: { 'Access-Control-Allow-Origin': '*' },
    host,
    port,
    proxy,
    hot,
    historyApiFallback: {
      // Paths with dots should still use the history fallback.
      // See https://github.com/facebook/create-react-app/issues/387.
      disableDotRule: true,
    },
    static: {
      // By default WebpackDevServer serves physical files from current directory
      // in addition to all the virtual build products that it serves from memory.
      // This is confusing because those files won’t automatically be available in
      // production build folder unless we copy them. However, copying the whole
      // project directory is dangerous because we may expose sensitive files.
      // Instead, we establish a convention that only files in `public` directory
      // get served. Our build script will copy `public` into the `build` folder.
      // In `index.html`, you can get URL of `public` folder with %PUBLIC_URL%:
      // <link rel="shortcut icon" href="%PUBLIC_URL%/favicon.ico">
      // In JavaScript code, you can access it with `process.env.PUBLIC_URL`.
      // Note that we only recommend to use `public` folder as an escape hatch
      // for files like `favicon.ico`, `manifest.json`, and libraries that are
      // for some reason broken when imported through Webpack. If you just want to
      // use an image, put it in `src` and `import` it from JavaScript instead.
      directory: paths.appPublic,
      // Reportedly, this avoids CPU overload on some systems.
      // https://github.com/facebook/create-react-app/issues/293
      // src/node_modules is not ignored to support absolute imports
      // https://github.com/facebook/create-react-app/issues/1065
      watch: {
        ignored: ignoredFiles(paths.appSrc),
      },
    },
    setupMiddlewares(middlewares, devServer) {
      if (fs.existsSync(paths.proxySetup)) {
        // This registers user provided middleware for proxy reasons
        require(paths.proxySetup)(devServer.app);
      }

      middlewares.push(
        // This lets us fetch source contents from webpack for the error overlay
        evalSourceMapMiddleware(devServer),
        // This lets us open files from the runtime error overlay.
        errorOverlayMiddleware(),
        // This service worker file is effectively a 'no-op' that will reset any
        // previous service worker registered for the same host:port combination.
        // We do this in development to avoid hitting the production cache if
        // it used the same host and port.
        // https://github.com/facebook/create-react-app/issues/2272#issuecomment-302832432
        noopServiceWorkerMiddleware('/')
      );

      return middlewares;
    },
  };
};
